from sharedlib import shared_function


def handler(event, context):
    response = {
        "statusCode": 200,
        "body": '*** SERVICE 1 *** - shared_function(5, 7) = ' + str(shared_function(5, 7))
    }

    return response
