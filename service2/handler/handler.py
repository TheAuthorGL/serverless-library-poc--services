from sharedlib import shared_function


def handler(event, context):
    response = {
        "statusCode": 200,
        "body": '*** SERVICE 2 *** - shared_function(2, 3) = ' + str(shared_function(2, 3))
    }

    return response
