# Sharing Python Libraries accross multiple Serverless Microservices

## Problem
Let's say we have two serverless microservices using a basic python & pipenv stack, `service-1` and `service-2`. These two services both make use of the same library, `shared_library`, which is currently being copied into the root directory of each of the services.

Our problem begins when _Developer A_, who maintains `service-1`, makes a change to `shared-library`, because now _Developer B_, who maintains `service-2` needs to copy, paste and then commit the changes to that service.

## Suggested Solution

- Separate `shared-library` into its own private repository; Usually we would create a public PIP package, but we won't, since we're assuming `shared-library` may contain non-generic, sensitive information that we do not want to expose to the public.
- Use PIP to require `shared-library` as a repo-sourced dependency in each of the services.
